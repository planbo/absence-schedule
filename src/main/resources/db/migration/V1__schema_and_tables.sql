CREATE TABLE public.person
(
    id integer NOT NULL,
    first_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    middle_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "position" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT person_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE public.absence_event
(
    id integer NOT NULL,
    date timestamp without time zone,
    reason character varying(255) COLLATE pg_catalog."default",
    person_id integer,
    CONSTRAINT absence_event_pkey PRIMARY KEY (id),
    CONSTRAINT fkst211w46qjk4jhrq2dvjty4vb FOREIGN KEY (person_id)
        REFERENCES public.person (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

