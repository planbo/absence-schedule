package ru.pab.absence.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pab.absence.entity.AbsenceEvent;
import ru.pab.absence.entity.Person;
import ru.pab.absence.service.AbsenceScheduleService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Platunov
 */

@RestController
public class UtilController {

    @Autowired
    AbsenceScheduleService absenceScheduleService;

    @RequestMapping(value = "/person/search", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> personSearch(@RequestParam(value = "lastName", required = false) String lastName,
                                                           @RequestParam(value = "firstName", required = false) String firstName,
                                                           @RequestParam(value = "middleName", required = false) String middleName,
                                                           @RequestParam(value = "position", required = false) String position) {
        List<Person> persons = absenceScheduleService.searchPersonByParams(lastName, firstName, middleName, position);
        Map<String, Object> result = new HashMap<>();
        result.put("persons", persons);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/person/delete", method = RequestMethod.DELETE)
    public void personDelete(@RequestParam(value = "personId") int personId) {
        absenceScheduleService.deletePerson(personId);
    }

    @RequestMapping(value = "/person/add", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> personAdd(@RequestBody Person person) {
        Map<String, Object> result = new HashMap<>();
        absenceScheduleService.addPerson(person);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/person/edit", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> personEdit(@RequestBody Person person) {
        Map<String, Object> result = new HashMap<>();
        absenceScheduleService.editPerson(person);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/event/search-by-person-id", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> eventSearchByPersonId(@RequestParam(value = "personId") int personId) {
        Map<String, Object> result = new HashMap<>();
        List<AbsenceEvent> events = absenceScheduleService.searchEventsByPersonId(personId);
        result.put("events", events);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "event/search", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> eventSearch(@RequestParam(value = "lastName", required = false) String lastName,
                                                           @RequestParam(value = "firstName", required = false) String firstName,
                                                           @RequestParam(value = "middleName", required = false) String middleName,
                                                           @RequestParam(value = "position", required = false) String position,
                                                           @RequestParam(value = "dateFrom", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateFrom,
                                                           @RequestParam(value = "dateTo", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateTo,
                                                           @RequestParam(value = "reason", required = false) String reason) {

        Map<String, Object> result = new HashMap<>();
        List<AbsenceEvent> events = absenceScheduleService.searchEvents(lastName, firstName, middleName, position, dateFrom, dateTo, reason);
        result.put("events", events);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/event/delete", method = RequestMethod.DELETE)
    public void eventDelete(@RequestParam(value = "eventId") int eventId) {
        absenceScheduleService.deleteEvent(eventId);
    }

    @RequestMapping(value = "/event/add", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> eventAdd(@RequestBody AbsenceEvent event) {
        Map<String, Object> result = new HashMap<>();
        absenceScheduleService.addEvent(event);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/event/edit", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> eventEdit(@RequestBody AbsenceEvent event) {
        Map<String, Object> result = new HashMap<>();
        absenceScheduleService.editEvent(event);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
