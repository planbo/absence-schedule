package ru.pab.absence.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.pab.absence.service.AbsenceScheduleService;

/**
 * @author Andrey Platunov
 */

@Controller
public class MainController {

    @Autowired
    AbsenceScheduleService absenceScheduleService;

    @RequestMapping(value = "/absence-schedule", method = RequestMethod.GET)
    public ModelAndView getIndexPage(Model model) {
        return new ModelAndView("index");
    }
}
