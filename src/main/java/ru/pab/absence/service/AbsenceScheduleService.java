package ru.pab.absence.service;

import ru.pab.absence.entity.AbsenceEvent;
import ru.pab.absence.entity.Person;

import java.util.Date;
import java.util.List;

/**
 * @author Andrey Platunov
 */
public interface AbsenceScheduleService {

    List<Person> searchPersonByParams(String lastName, String firstName, String middleName, String position);

    void deletePerson(int personId);

    void addPerson(Person person);

    void editPerson(Person person);

    void deleteEvent(int eventId);

    void addEvent(AbsenceEvent event);

    void editEvent(AbsenceEvent event);

    List<AbsenceEvent> searchEventsByPersonId(int personId);

    List<AbsenceEvent> searchEvents(String lastName,
                                    String firstName,
                                    String middleName,
                                    String position,
                                    Date dateFrom,
                                    Date dateTo,
                                    String reason);
}
