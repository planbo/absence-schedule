package ru.pab.absence.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pab.absence.entity.AbsenceEvent;
import ru.pab.absence.entity.Person;
import ru.pab.absence.repository.AbsenceEventDao;
import ru.pab.absence.repository.PersonDao;

import java.util.Date;
import java.util.List;

/**
 * @author Andrey Platunov
 */

@Service
public class AbsenceScheduleServiceImpl implements AbsenceScheduleService {

    @Autowired
    private PersonDao personDao;

    @Autowired
    private AbsenceEventDao absenceEventDao;

    @Transactional
    @Override
    public List<Person> searchPersonByParams(String lastName, String firstName, String middleName, String position) {
        return personDao.findPersonByParams(lastName,
                firstName,
                middleName,
                position);
    }

    @Transactional
    @Override
    public void deletePerson(int personId) {
        personDao.deletePersonById(personId);
    }

    @Transactional
    @Override
    public void addPerson(Person person) {
        personDao.addPerson(person);
    }

    @Override
    public void editPerson(Person person) {
        personDao.editPerson(person);
    }

    @Override
    public void deleteEvent(int eventId) {
        absenceEventDao.deleteEventById(eventId);
    }

    @Override
    public void addEvent(AbsenceEvent event) {
        absenceEventDao.addEvent(event);
    }

    @Override
    public void editEvent(AbsenceEvent event) {
        absenceEventDao.editEvent(event);
    }

    @Transactional
    @Override
    public List<AbsenceEvent> searchEventsByPersonId(int personId) {
        return absenceEventDao.getAbsenceEventByPersonId(personId);
    }

    @Override
    public List<AbsenceEvent> searchEvents(String lastName,
                                           String firstName,
                                           String middleName,
                                           String position,
                                           Date dateFrom,
                                           Date dateTo,
                                           String reason) {
        return absenceEventDao.getAbsenceEvent(lastName, firstName, middleName, position, dateFrom, dateTo, reason);
    }
}
