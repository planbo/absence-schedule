package ru.pab.absence.repository;

import org.springframework.stereotype.Repository;
import ru.pab.absence.entity.Person;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Platunov
 */

@Repository
public class PersonDaoImpl implements PersonDao {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public List<Person> findPersonByParams(String lastName, String firstName, String middleName, String position) {

        StringBuilder queryBuilder = new StringBuilder();
        Map<String, Serializable> properties = new HashMap<>();

        queryBuilder.append("SELECT a FROM Person a");

        Boolean isFirstCondition = true;

        if (lastName != null) {
            appendWhereOrEndString(queryBuilder, isFirstCondition);
            isFirstCondition = false;
            queryBuilder.append(" a.lastName LIKE :lastName");
            properties.put("lastName", "%" + lastName + "%");
        }

        if (firstName != null) {
            appendWhereOrEndString(queryBuilder, isFirstCondition);
            isFirstCondition = false;
            queryBuilder.append(" a.firstName LIKE :firstName");
            properties.put("firstName", "%" + firstName + "%");
        }

        if (middleName != null) {
            appendWhereOrEndString(queryBuilder, isFirstCondition);
            isFirstCondition = false;
            queryBuilder.append(" a.middleName LIKE :middleName");
            properties.put("middleName", "%" +  middleName + "%");
        }

        if (position != null) {
            appendWhereOrEndString(queryBuilder, isFirstCondition);
            queryBuilder.append(" a.position LIKE :position");
            properties.put("position", "%" +  position + "%");
        }

        queryBuilder.append(" ORDER BY a.lastName, a.firstName, a.middleName");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery(queryBuilder.toString());
        for (Map.Entry<String, Serializable> property : properties.entrySet()) {
            query.setParameter(property.getKey(), property.getValue());
        }
        List<Person> result = query.getResultList();
        entityManager.close();

        return result;
    }

    @Override
    public boolean deletePersonById(int id) {
        boolean result = false;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        Person person = entityManager.find(Person.class, id);
        if (person != null) {
            entityManager.remove(person);
            entityManager.getTransaction().commit();
            result = true;
        }
        entityManager.close();
        return result;
    }

    @Override
    public boolean addPerson(Person person) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (person != null) {
            entityManager.getTransaction().begin();
            entityManager.persist(person);
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        }
        return false;
    }

    @Override
    public boolean editPerson(Person person) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (person != null) {
            entityManager.getTransaction().begin();
            entityManager.merge(person);
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        }
        return false;
    }

    private void appendWhereOrEndString(StringBuilder queryBuilder, Boolean isFirstCondition) {
        if (isFirstCondition) {
            queryBuilder.append(" WHERE");
        } else {
            queryBuilder.append(" AND");
        }
    }
}
