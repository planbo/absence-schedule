package ru.pab.absence.repository;

import org.springframework.stereotype.Repository;
import ru.pab.absence.entity.AbsenceEvent;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Platunov
 */

@Repository
public class AbsenceEventDaoImpl implements AbsenceEventDao {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public List<AbsenceEvent> getAbsenceEventByPersonId(int personId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("FROM AbsenceEvent WHERE person_id = :personId");
        query.setParameter("personId", personId);
        List<AbsenceEvent> result = query.getResultList();
        entityManager.close();
        return result;
    }

    @Override
    public List<AbsenceEvent> getAbsenceEvent(String lastName,
                                              String firstName,
                                              String middleName,
                                              String position,
                                              Date dateFrom,
                                              Date dateTo,
                                              String reason) {

        StringBuilder queryBuilder = new StringBuilder();
        Map<String, Serializable> properties = new HashMap<>();

        queryBuilder.append("SELECT a FROM AbsenceEvent a INNER JOIN Person b ON a.person.id = b.id");

        if (lastName != null) {
            queryBuilder.append(" AND b.lastName LIKE :lastName");
            properties.put("lastName", "%" + lastName + "%");
        }

        if (firstName != null) {
            queryBuilder.append(" AND b.firstName LIKE :firstName");
            properties.put("firstName", "%" + firstName + "%");
        }

        if (middleName != null) {
            queryBuilder.append(" AND b.middleName LIKE :middleName");
            properties.put("middleName", "%" + middleName + "%");
        }

        if (position != null) {
            queryBuilder.append(" AND b.position LIKE :position");
            properties.put("position", "%" + position + "%");
        }

        if (dateFrom != null) {
            queryBuilder.append(" AND a.date >= :dateFrom");
            properties.put("dateFrom", dateFrom);
        }

        if (dateTo != null) {
            queryBuilder.append(" AND a.date <= :dateTo");
            properties.put("dateTo", dateTo);
        }

        if (reason != null) {
            queryBuilder.append(" AND a.reason LIKE :reason");
            properties.put("reason", "%" +  reason + "%");
        }

        queryBuilder.append(" ORDER BY a.person.lastName, a.person.firstName, a.person.middleName, a.date");

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery(queryBuilder.toString());
        for (Map.Entry<String, Serializable> property : properties.entrySet()) {
            query.setParameter(property.getKey(), property.getValue());
        }
        List<AbsenceEvent> result = query.getResultList();
        entityManager.close();

        return result;
    }

    @Override
    public boolean deleteEventById(int id) {
        boolean result = false;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        AbsenceEvent event = entityManager.find(AbsenceEvent.class, id);
        if (event != null) {
            entityManager.remove(event);
            entityManager.getTransaction().commit();
            result = true;
        }
        entityManager.close();
        return result;
    }

    @Override
    public boolean addEvent(AbsenceEvent event) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (event != null) {
            entityManager.getTransaction().begin();
            entityManager.persist(event);
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        }
        return false;
    }

    @Override
    public boolean editEvent(AbsenceEvent event) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (event != null) {
            entityManager.getTransaction().begin();
            entityManager.merge(event);
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        }
        return false;
    }
}
