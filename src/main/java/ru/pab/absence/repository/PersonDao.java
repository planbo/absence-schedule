package ru.pab.absence.repository;

import ru.pab.absence.entity.Person;

import java.util.List;

/**
 * @author Andrey Platunov
 */
public interface PersonDao {

    List<Person> findPersonByParams(String lastName, String firstName, String middleName, String position);

    boolean deletePersonById(int id);

    boolean addPerson(Person person);

    boolean editPerson(Person person);
}
