package ru.pab.absence.repository;

import ru.pab.absence.entity.AbsenceEvent;

import java.util.Date;
import java.util.List;

/**
 * @author Andrey Platunov
 */
public interface AbsenceEventDao {

    List<AbsenceEvent> getAbsenceEventByPersonId(int personId);

    List<AbsenceEvent> getAbsenceEvent(String lastName,
                                       String firstName,
                                       String middleName,
                                       String position,
                                       Date dateFrom,
                                       Date dateTo,
                                       String reason);

    boolean deleteEventById(int id);

    boolean addEvent(AbsenceEvent event);

    boolean editEvent(AbsenceEvent event);
}
