package ru.pab.absence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.pab.absence.config.JpaConfig;
import ru.pab.absence.config.SpringMvcConfig;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(new Class<?>[] {
				Application.class,
				JpaConfig.class,
				SpringMvcConfig.class
		}, args);
	}
}
