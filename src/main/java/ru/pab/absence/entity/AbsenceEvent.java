package ru.pab.absence.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Andrey Platunov
 */

@Entity
@Table(name = "absence_event")
public class AbsenceEvent {

    @Id
    @Column (name = "id")
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "date")
    Date date;

    @Column(name = "reason")
    String reason;

    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
