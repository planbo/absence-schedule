<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html ng-app="absSchApp">
<head>
    <script defer src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script defer src="/js/index.js"></script>
    <meta charset="utf-8">
    <title>Табель отсутствия</title>
</head>
<body ng-app="absSchApp" class="ng-cloak">
<div ng-controller="absSchCtrl">
    <h2>Табель отсутствия</h2>
    <c:url var="addImgUrl" value="../img/add.png" />
    <c:url var="editImgUrl" value="../img/edit.png" />
    <c:url var="deleteImgUrl" value="../img/delete.png" />
    <c:url var="searchImgUrl" value="../img/search.png" />

    <table>
        <tr>
            <td>Id:</td>
            <td><input type="text" ng-model="personEditId" disabled="true"></td>
        </tr>
        <tr>
            <td>Фамилия:</td>
            <td><input type="text" ng-model="personEditLastName"></td>
        </tr>
        <tr>
            <td>Имя:</td>
            <td><input type="text" ng-model="personEditFirstName"></td>
        </tr>
        <tr>
            <td>Отчество:</td>
            <td><input type="text" ng-model="personEditMiddleName"></td>
        </tr>
        <tr>
            <td>Должность:</td>
            <td><input type="text" ng-model="personEditPosition"></td>
        </tr>
    </table>

    <p>
        <div ng-if="personEditId == null">
            <button ng-click="sendAddPerson()">Создать пользователя</button>
        </div>
        <div ng-if="personEditId != null">
            <button ng-click="sendEditPerson()">Изменить пользователя</button>
        </div>
    </p>

    <p>
        <button ng-click="clearPersonForm()">Очистить форму</button>
    </p>

    <table style="border: 1px solid; width: 100%; text-align:center">
        <thead style="background:#d3dce3">
        <tr>
            <th colspan="5">Поиск пользователя</th>
        </tr>
        <tr>
            <th>Фaмилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Должность</th>
            <th>Действия</th>
        </tr>
        <tr>
            <th><input type="text" ng-model="personSearchLastName"></th>
            <th><input type="text" ng-model="personSearchFirstName"></th>
            <th><input type="text" ng-model="personSearchMiddleName"></th>
            <th><input type="text" ng-model="personSearchPosition"></th>
            <th>
                <input type="image" src="${searchImgUrl}" title="Искать по фильтру" ng-click="searchPerson()"/>
            </th>
        </tr>
        <tr ng-repeat="person in persons | startFrom: startingPerson() | limitTo: personPerPage">
            <th><label ng-bind="person.lastName"/></th>
            <th><label ng-bind="person.firstName"/></th>
            <th><label ng-bind="person.middleName"/></th>
            <th><label ng-bind="person.position"/></th>
            <th>
                <input type="image" src="${addImgUrl}" title="Создать событие для пользователя" ng-click="addEvent(person)"/>
                <input type="image" src="${editImgUrl}" title="Редактировать пользователя" ng-click="editPerson(person)"/>
                <input type="image" src="${deleteImgUrl}" title="Удалить пользователя" ng-click="deletePerson(person.id)"/>
                <input type="image" src="${searchImgUrl}" title="Показать события отсутствия" ng-click="searchEventByPersonId(person)"/>
            </th>
        </tr>
        </thead>
    </table>

    <p>
        <button ng-click="personZeroPage()" ng-disabled="personPageNumber == 0">&lt;&lt;</button>
        <button ng-click="personPrevPage()" ng-disabled="personPageNumber == 0">&lt;</button>
        <button ng-click="personNextPage()" ng-disabled="personPageNumber == personCountPages() - 1">&gt;</button>
        <button ng-click="personLastPage()" ng-disabled="personPageNumber == personCountPages() - 1">&gt;&gt;</button>
        <text> Страница {{personPageNumber + 1}} из {{personCountPages()}}</text>
    </p>

    <p/>

    <table>
        <tr>
            <td>Id пользователя:</td>
            <td><input type="text" ng-model="eventEditPersonId" disabled="true"></td>
        </tr>
        <tr>
            <td>Фамилия:</td>
            <td><input type="text" ng-model="eventEditLastName" disabled="true"></td>
        </tr>
        <tr>
            <td>Имя:</td>
            <td><input type="text" ng-model="eventEditFirstName" disabled="true"></td>
        </tr>
        <tr>
            <td>Отчество:</td>
            <td><input type="text" ng-model="eventEditMiddleName" disabled="true"></td>
        </tr>
        <tr>
            <td>Должность:</td>
            <td><input type="text" ng-model="eventEditPosition" disabled="true"></td>
        </tr>
        <tr>
            <td>Id события:</td>
            <td><input type="text" ng-model="eventEditId" disabled="true"></td>
        </tr>
        <tr>
            <td>Дата и время:</td>
            <td><input type="datetime-local" ng-model="eventEditDate"></td>
        </tr>
        <tr>
            <td>Причина:</td>
            <td><input type="text" ng-model="eventEditReason"></td>
        </tr>
    </table>

    <p>
        <div ng-if="eventEditId == null">
            <button ng-click="sendAddEvent()">Создать событие</button>
        </div>
        <div ng-if="eventEditId != null">
            <button ng-click="sendEditEvent()">Изменить событие</button>
        </div>
    </p>

    <p>
        <button ng-click="clearEventForm()">Очистить форму</button>
    </p>

    <table style="border: 1px solid; width: 100%; text-align:center">
        <thead style="background:#d3dce3">
        <tr>
            <th colspan="7">Поиск событий отсутствия</th>
        </tr>
        <tr>
            <th>Фaмилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Должность</th>
            <th>Дата и время</th>
            <th>Причина</th>
            <th>Действия</th>
        </tr>
        <tr>
            <th><input type="text" ng-model="eventSearchLastName"></th>
            <th><input type="text" ng-model="eventSearchFirstName"></th>
            <th><input type="text" ng-model="eventSearchMiddleName"></th>
            <th><input type="text" ng-model="eventSearchPosition"></th>
            <th>от <input type="datetime-local" ng-model="eventSearchDateFrom"> до <input type="datetime-local" ng-model="eventSearchDateTo"></th>
            <th><input type="text" ng-model="eventSearchReason"></th>
            <th>
                <input type="image" src="${searchImgUrl}" title="Искать по фильтру" ng-click="searchEvent()"/>
            </th>
        </tr>
        <tr ng-repeat="event in events | startFrom: startingEvent() | limitTo: eventPerPage">
            <th><label ng-bind="event.person.lastName"/></th>
            <th><label ng-bind="event.person.firstName"/></th>
            <th><label ng-bind="event.person.middleName"/></th>
            <th><label ng-bind="event.person.position"/></th>
            <th><label ng-bind="event.date | date:'dd/MM/yyyy HH:mm'"/></th>
            <th><label ng-bind="event.reason"/></th>
            <th>
                <input type="image" src="${editImgUrl}" title="Редактировать событие" ng-click="editEvent(event)"/>
                <input type="image" src="${deleteImgUrl}" title="Удалить событие" ng-click="deleteEvent(event.id)"/>
            </th>
        </tr>
        </thead>
    </table>

    <p>
        <button ng-click="eventZeroPage()" ng-disabled="eventPageNumber == 0">&lt;&lt;</button>
        <button ng-click="eventPrevPage()" ng-disabled="eventPageNumber == 0">&lt;</button>
        <button ng-click="eventNextPage()" ng-disabled="eventPageNumber == eventCountPages() - 1">&gt;</button>
        <button ng-click="eventLastPage()" ng-disabled="eventPageNumber == eventCountPages() - 1">&gt;&gt;</button>
        <text> Страница {{eventPageNumber + 1}} из {{eventCountPages()}}</text>
    </p>
</div>
</body>
</html>