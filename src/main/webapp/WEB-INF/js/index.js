var absSchModule = angular.module("absSchApp", []);

absSchModule.controller("absSchCtrl", function ($scope, $http, $window) {

    $scope.persons = [];
    $scope.events = [];

    $scope.personSearchLastName = "";
    $scope.personSearchFirstName = "";
    $scope.personSearchMiddleName = "";
    $scope.personSearchPosition = "";

    $scope.eventSearchLastName = "";
    $scope.eventSearchFirstName = "";
    $scope.eventSearchMiddleName = "";
    $scope.eventSearchPosition = "";
    $scope.eventSearchDateFrom = null;
    $scope.eventSearchDateTo = null;
    $scope.eventSearchReason = "";

    $scope.personEditId = null;
    $scope.personEditLastName = "";
    $scope.personEditFirstName = "";
    $scope.personEditMiddleName = "";
    $scope.personEditPosition = "";

    $scope.eventEditPersonId = null;
    $scope.eventEditLastName = "";
    $scope.eventEditFirstName = "";
    $scope.eventEditMiddleName = "";
    $scope.eventEditPosition = "";
    $scope.eventEditId = null;
    $scope.eventEditDate = null;
    $scope.eventEditReason = "";

    $scope.personPageNumber = 0;
    $scope.personPerPage = 5;

    $scope.eventPageNumber = 0;
    $scope.eventPerPage = 5;

    $scope.startingPerson = function() {
        return $scope.personPageNumber * $scope.personPerPage;
    };

    $scope.personZeroPage = function() {
        $scope.personPageNumber = 0;
    };

    $scope.personPrevPage = function() {
        if ($scope.personPageNumber > 0) {
            $scope.personPageNumber--;
        }
    };

    $scope.personNextPage = function() {
        if ($scope.personPageNumber < $scope.personCountPages() - 1) {
            $scope.personPageNumber++;
        }
    };

    $scope.personLastPage = function() {
        var ceil = Math.ceil($scope.persons.length / $scope.personPerPage);
        ceil--;
        $scope.personPageNumber = ceil;
    };

    $scope.personCountPages = function() {
        var ceil = Math.ceil($scope.persons.length / $scope.personPerPage);
        if (ceil == 0) {
            return 1;
        } else {
            return ceil;
        }
    };

    $scope.startingEvent = function() {
        return $scope.eventPageNumber * $scope.eventPerPage;
    };

    $scope.eventZeroPage = function() {
        $scope.eventPageNumber = 0;
    };

    $scope.eventPrevPage = function() {
        if ($scope.eventPageNumber > 0) {
            $scope.eventPageNumber--;
        }
    };

    $scope.eventNextPage = function() {
        if ($scope.eventPageNumber < $scope.eventCountPages() - 1) {
            $scope.eventPageNumber++;
        }
    };

    $scope.eventLastPage = function() {
        var ceil = Math.ceil($scope.events.length / $scope.eventPerPage);
        ceil--;
        $scope.eventPageNumber = ceil;
    };

    $scope.eventCountPages = function() {
        var ceil = Math.ceil($scope.events.length / $scope.eventPerPage);
        if (ceil == 0) {
            return 1;
        } else {
            return ceil;
        }
    };

    $scope.checkPersonFormParams = function() {
        if ($scope.personEditLastName == ""
            || $scope.personEditFirstName == ""
            || $scope.personEditMiddleName == ""
            || $scope.personEditPosition == "") {
            alert ("Все доступные поля должны быть заполнены");
            return false;
        }
        return true;
    };

    $scope.checkEventFormParams = function() {
        if ($scope.eventEditPersonId == null) {
            alert ("Необходимо указать пользователя из справочника пользователей");
            return false;
        }

        if ($scope.eventEditDate == null
            || $scope.eventEditReason == "") {
            alert ("Все доступные поля должны быть заполнены");
            return false;
        }
        return true;
    };

    $scope.clearPersonForm = function() {
        $scope.personEditId = null;
        $scope.personEditLastName = "";
        $scope.personEditFirstName = "";
        $scope.personEditMiddleName = "";
        $scope.personEditPosition = "";
    };

    $scope.searchPerson = function() {
        $http({
            method: "GET",
            url: "/person/search",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            params: {
                "lastName": $scope.personSearchLastName,
                "firstName": $scope.personSearchFirstName,
                "middleName": $scope.personSearchMiddleName,
                "position": $scope.personSearchPosition
            }
        }).then(
            function(response) {
                $scope.persons = response.data.persons;
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.editPerson = function(person) {
        $scope.personEditId = person.id;
        $scope.personEditLastName = person.lastName;
        $scope.personEditFirstName = person.firstName;
        $scope.personEditMiddleName = person.middleName;
        $scope.personEditPosition = person.position;
    };

    $scope.sendAddPerson = function() {
        if (!$scope.checkPersonFormParams()) {
            return;
        }

        var person = {
            "lastName": $scope.personEditLastName,
            "firstName": $scope.personEditFirstName,
            "middleName": $scope.personEditMiddleName,
            "position": $scope.personEditPosition
        };

        $http({
            method: "POST",
            url: "/person/add",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            data: person
        }).then(
            function() {
                alert("Пользователь создан");
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.sendEditPerson = function() {
        if (!$scope.checkPersonFormParams()) {
            return;
        }

        var person = {
            "id": $scope.personEditId,
            "lastName": $scope.personEditLastName,
            "firstName": $scope.personEditFirstName,
            "middleName": $scope.personEditMiddleName,
            "position": $scope.personEditPosition
        };

        $http({
            method: "POST",
            url: "/person/edit",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            data: person
        }).then(
            function() {
                alert("Пользователь изменен");
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.deletePerson = function(personId) {
        $http({
            method: "DELETE",
            url: "/person/delete",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            params: {
                "personId": personId
            }
        }).then(
            function() {
                $scope.searchPerson();
                alert("Пользователь удален");
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.searchEventByPersonId = function(person) {

        $scope.eventSearchLastName = person.lastName;
        $scope.eventSearchFirstName = person.firstName;
        $scope.eventSearchMiddleName = person.middleName;
        $scope.eventSearchPosition = person.position;

        $http({
            method: "GET",
            url: "/event/search-by-person-id",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            params: {
                "personId": person.id
            }
        }).then(
            function(response) {
                $scope.events = response.data.events;
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.searchEvent = function() {

        $http({
            method: "GET",
            url: "/event/search",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            params: {
                "lastName": $scope.eventSearchLastName,
                "firstName": $scope.eventSearchFirstName,
                "middleName": $scope.eventSearchMiddleName,
                "position": $scope.eventSearchPosition,
                "dateFrom": $scope.eventSearchDateFrom,
                "dateTo": $scope.eventSearchDateTo,
                "reason": $scope.eventSearchReason
            }
        }).then(
            function(response) {
                $scope.events = response.data.events;
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.clearEventForm = function() {
        $scope.eventEditPersonId = null;
        $scope.eventEditLastName = "";
        $scope.eventEditFirstName = "";
        $scope.eventEditMiddleName = "";
        $scope.eventEditPosition = "";
        $scope.eventEditId = null;
        $scope.eventEditDate = null;
        $scope.eventEditReason = "";
    };

    $scope.addEvent = function(person) {
        $scope.eventEditPersonId = person.id;
        $scope.eventEditLastName = person.lastName;
        $scope.eventEditFirstName = person.firstName;
        $scope.eventEditMiddleName = person.middleName;
        $scope.eventEditPosition = person.position;
    };

    $scope.sendAddEvent = function() {
        if (!$scope.checkEventFormParams()) {
            return;
        }

        var event = {
            "person": {
                "id": $scope.eventEditPersonId,
                "lastName": $scope.eventEditLastName,
                "firstName": $scope.eventEditFirstName,
                "middleName": $scope.eventEditMiddleName,
                "position": $scope.eventEditPosition
            },
            "date": $scope.eventEditDate,
            "reason": $scope.eventEditReason,
            "id": $scope.eventEditId
        };

        $http({
            method: "POST",
            url: "/event/add",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            data: event
        }).then(
            function() {
                alert("Событие создано");
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.sendEditEvent = function() {
        if (!$scope.checkEventFormParams()) {
            return;
        }

        var event = {
            "person": {
                "id": $scope.eventEditPersonId,
                "lastName": $scope.eventEditLastName,
                "firstName": $scope.eventEditFirstName,
                "middleName": $scope.eventEditMiddleName,
                "position": $scope.eventEditPosition
            },
            "date": $scope.eventEditDate,
            "reason": $scope.eventEditReason,
            "id": $scope.eventEditId
        };

        $http({
            method: "POST",
            url: "/event/edit",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            data: event
        }).then(
            function() {
                alert("Событие изменено");
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.deleteEvent = function(eventId) {
        $http({
            method: "DELETE",
            url: "/event/delete",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            params: {
                "eventId": eventId
            }
        }).then(
            function() {
                $scope.searchEvent();
                alert("Событие удалено");
            },
            function(errResponse) {
                alert('Ошибка: ' + errResponse.toString());
            }
        );
    };

    $scope.editEvent = function(event) {
        $scope.eventEditPersonId = event.person.id;
        $scope.eventEditLastName = event.person.lastName;
        $scope.eventEditFirstName = event.person.firstName;
        $scope.eventEditMiddleName = event.person.middleName;
        $scope.eventEditPosition = event.person.position;
        $scope.eventEditId = event.id;
        $scope.eventEditDate = event.date;
        $scope.eventEditReason = event.reason;
    };

});

absSchModule.filter('startFrom', function () {
    return function (input, start) {
        start = +start;
        return input.slice(start);
    }
});