### What is this?
This is application implements absence events scheduling.

### How to create war file?
1. Clone current repo using git.
2. Make sure Java 8 and Maven installed.
3. Perform next command via console: 
    ```
    mvn clean package
    ```
4. By default result war is located at:
    ```
    <PROJECT_PATH>/target/absence-schedule-1.0.0-SNAPSHOT.war
    ```

###  Which params current war use?
1. By default app connect to database with next properties:
    1. JDBC driver: PostgreSQL
    2. Host: localhost
    3. Port: 5432
    4. DB username: postgres
    5. DB password: postgres
2. To override connection properties:
    1. Open file by next path
        ```
        <PROJECT_PATH>/src/main/resources/application.properties
        ```
    2. Override next properties:
        1. JDBC driver:
            ```
            db.driverClassName=org.postgresql.Driver=org.postgresql.Driver
            db.url=jdbc:postgresql://localhost:5432/
            ```
        2. Host and port:
            ```
            db.url=jdbc:postgresql://localhost:5432/
            ```
        3. DB username:
            ```    
            db.username=postgres
            ```
        4. DB password:
            ```
            db.password=postgres
            ```
    3. Build war then deploy it to server.
    
### How to migrate DB?
1. By default flyway plugin connect to database with next properties:
    1. JDBC driver: PostgreSQL
    2. Host: localhost
    3. Port: 5432
    4. DB username: postgres
    5. DB password: postgres
2. Connection properties overriding at pom.xml (see <plugins> block).
3. To migrate DB, perform next command:
    ```
    mvn flyway-migrate
    ```

    